// Folders
/html/ --> Working directory Non build html templates
/build/ --> Folder where html will be build for development
/dist/ --> Folder for distribution (minifying etc)


// Grunt
Run `grunt` to start with prompts (questions) to help you with the right deployment options
Run `grunt server` to build your templates to the `build/` folder with a local server with livereload and watchers (this wil be automatically done when you run `yo tamtam`
Run `grunt dist` to build and minify your project to the `dist/` folder for distribution to production server
Run `grunt dist:server` to build and minify your project to the `dist/` folder for distribution to production server with a local server with livereload and watchers


Use 'grunt bump' to patch your version nummer (0.0.1+) which is done by every 'grunt dist' commando
Use 'grunt bump:minor' to patch your version number (0.1+.0)
Use 'grunt bump:major' to patch your version number (1+.0.0)

