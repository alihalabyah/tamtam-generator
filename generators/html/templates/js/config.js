var <%= computedProjectName %> = (function appConfig (<%= computedProjectName %>, window, document, undefined) {

	var baseUrl = window.location.protocol + '//' + window.location.host;

	<%= computedProjectName %>.Config = {
	name : '<%= projectName %>',
	computedName : '<%= computedProjectName %>'
	};

	return <%= computedProjectName %>;

}(<%= computedProjectName %> || {}, this, this.document));
