'use strict';

var semver = require('semver');

module.exports = function (grunt) {

    var config = {
        appName : '<%= projectName %>',
        currentVersion : grunt.file.readJSON('package.json').version
    };

    var env = {
        build: 'build',
        dist: 'dist'
    };

    // configurable paths
    var path = {
        root: '.',
	    includes: 'includes',
	    views: 'views',
        tmp: '.tmp',
	    bower: 'bower_components',
        html: 'html',
        images: 'images',
        scripts: 'js',
        css: 'css',
        sass: 'sass'<% if(includeTesting){%>,
        test: 'test'<%}%>
    };

	// show elapsed time at the end
	// Speedup load all grunt tasks
	require('jit-grunt')(grunt, {
		'includereplace': 'grunt-include-replace',
		'useminPrepare': 'grunt-usemin',<% if (includeGruntRev) { %>
		'rev' : 'grunt-rev',<% } %>
        'bump' : 'grunt-bump',
        'prompt': 'grunt-prompt'<% if (includeTesting) { %>,
        'protractor' : 'grunt-protractor-runner',
        'protractor_webdriver' : 'grunt-protractor-webdriver',
        'karma' : 'grunt-karma' <%}%>
    });

	grunt.initConfig({

        config : config,
        env : env,
        path: path,
        pkg: grunt.file.readJSON('package.json'),

        prompt: {
            global: {
                options : {
                    questions : [
                        {
                            config:  'buildtype',
                            type:    'list',
                            message: 'Where would you like grunt to build your project?',
                            default: 'server',
                            choices: [
                                {
                                    value: 'server',
                                    name:  'Build for development and start a local webserver'
                                },
                                {
                                    value: 'dist',
                                    name:  'Build for distribution'
                                }
                            ]
                        },
                        {
                            config:  'localserver',
                            type:    'confirm',
                            message: 'Would you like to run a local webserver afterwards, to test the distribution build?',
                            default: false,
                            when : function(answers) {
                                return answers.buildtype == 'dist';
                            }
                        }
                    ],
                    then: function(results, done) {
                        registerGruntTask(grunt.config('buildtype'),grunt.config('localserver'));
                        done();
                        return true;
                    }
                }
            },

            dist: {
                options: {
                    questions: [
                        {
                            config:  'bump.increment',
                            type:    'list',
                            message: 'Bump version from ' + config.currentVersion + ' to:',
                            default: 'patch',
                            choices: [
                                {
                                    value: 'patch',
                                    name:  'Patch:  ' + semver.inc(config.currentVersion, 'patch') + ' Backwards-compatible bug fixes.'
                                },
                                {
                                    value: 'minor',
                                    name:  'Minor:  ' + semver.inc(config.currentVersion, 'minor') + ' Add functionality in a backwards-compatible manner.'
                                },
                                {
                                    value: 'major',
                                    name:  'Major:  ' + semver.inc(config.currentVersion, 'major') + ' Incompatible API changes.'
                                },
                                {
                                    value: 'no-bump',
                                    name:  'Don\'t bump this time'
                                }
                            ]
                        }
                    ],
                    then: function(results, done) {
                        var version = grunt.config('bump.increment');
                        if (version !== 'no-bump') {
                            grunt.task.run(['bump:' + version ]);
                        }
                        done();
                        return true;
                    }
                }
            }
        },

        watch: {
	        options: {
		        interrupt: true,
		        debounceDelay: 1000
	        },
			sass: {
                files: [
                    '<%%= path.sass %>/**/*.scss',
                    '<%%= path.bower %>/bootstrap/sass/*.scss'
                ],
                tasks: ['sass:server']
            },
            styles: {
                files: ['<%%= env.build %>/<%%= path.css %>/**/*.css'],
                tasks: ['autoprefixer']
            },
	        images: {
		        files: ['<%%= path.images %>/**'],
		        tasks: ['newer:copy:images']
	        },
            scripts: {
                files: ['<%%= path.scripts %>/**/*.js'],
                tasks: ['newer:copy:scripts']
            },

            includereplace: {
                files: ['<%%= path.html %>/**/*.html'],
                tasks : ['includereplace:server']
            },

            livereload: {
                options: {
                    livereload: '<%%= connect.options.livereload %>'
                },
                files: [
                    '<%%= env.build %>/**/*.html',
                    '<%%= env.build %>/<%%= path.css %>/main.css',
                    '<%%= env.build %>/<%%= path.scripts %>/**/*.js',
                    '<%%= env.build %>/<%%= path.images %>/**/*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },

        includereplace: {
            server : {
                options: {
                    globals : {
                        'version' : '<%%= pkg.version %>'
                    },
                    includesDir: '<%%= path.html %>/<%%= path.includes %>/'
                },
                src: ['**/*.html','!<%%= path.includes %>/**'],
                dest: '<%%= env.build %>/',
                expand: true,
                cwd: '<%%= path.html %>/'
            },
            dist: {
                options: {
                    globals : {
                        'version' : '<%%= pkg.version %>'
                    },
                    includesDir: '<%%= path.html %>/<%%= path.includes %>/'
                },
                src: ['**/*.html','!<%%= path.includes %>/**'],
                dest: '<%%= env.dist %>/',
                expand: true,
                cwd: '<%%= path.html %>/'
            }
        },

		<% if (includeGruntRev) { %>rev: {
            options: {
                algorithm: 'md5',
                length: 8
            },
            assets: {
                files: [{
                    src: [
                        '<%%= env.dist %>/<%%= path.css %>/{,*/}*.css',
                        '<%%= env.dist %>/<%%= path.scripts %>/{,*/}*.js',
                        '<%%= env.dist %>/fonts/**/*.{eot,svg,ttf,woff}'
                    ]
                }]
            }
        },
        <% } %>
        bump: {
            options: {
                files: ['bower.json','package.json'],
                commit: false,
                createTag: false,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: false
            }
        },

        connect: {

	        options: {
		        port: 9000,
		        livereload: 35729,
		        //0.0.0.0 allows connections from external
		        hostname: '0.0.0.0',
		        directory: '<%%= env.build %>'
	        },

	        livereload: {
		        options: {
			        open: 'http://<%%= connect.options.hostname %>:<%%= connect.options.port %>/<%%= connect.options.open %>',
			        base: [
				        '<%%= env.build %>',
				        '<%%= path.tmp %>',
				        '<%%= path.root %>'
			        ]
		        }
	        },

	        dist: {
		        options: {
			        open: 'http://<%%= connect.options.hostname %>:<%%= connect.options.port %>/<%%= connect.options.open %>',
			        base : [
				        '<%%= env.dist %>',
				        '<%%= path.tmp %>',
				        '<%%= path.root %>'
			        ]
		        }
	        }
        },

        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '<%%= path.tmp %>',
                        '<%%= env.dist %>/*',
                        '!<%%= env.dist %>/.git*'
                    ]
                }]
            },
            server: {
                files : [{
                    src: ['<%%= path.tmp %>', '<%%= env.build %>']
                }]
            }
        },

        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: ['Gruntfile.js']
        },

        //This task requires you to have Ruby and Sass installed. If you're on OS X or Linux you probably already have Ruby installed
        //test with ruby -v in your terminal. When you've confirmed you have Ruby installed, run 'sudo gem install sass' to install Sass.
        sass: {
            options: {
                loadPath: ['<%%= path.bower %>','<%%= path.sass %>']
            },
            dist: {
                files: {
                    '<%%= env.build %>/<%%= path.css %>/main.css': '<%%= path.sass %>/main.scss'
                }
            },
            server: {
                files: {
                    '<%%= env.build %>/<%%= path.css %>/main.css': '<%%= path.sass %>/main.scss'
                }
            }
        },

        autoprefixer: {
            options: {
                browsers: ['last 2 version', 'ie 8', 'ie 9', 'ff > 14']
            },
            server: {
                files: [{
                    expand: true,
                    src: '<%%= env.build %>/<%%= path.css %>/{,*/}*.css'
                }]
            },
            dist: {
                files: [{
                    expand: true,
	                src: '<%%= env.dist %>/<%%= path.css %>/{,*/}*.css'
                }]
            }
        },

        useminPrepare: {
            options: {
                dest: '<%%= env.dist %>',
                root: '<%%= path.root %>'
            },
            html: '<%%= path.html %>/<%%= path.includes %>/{,*/}*.html'
        },

        usemin: {
            options: {
                assetsDirs: ['<%%= env.dist %>']
            },
            html: ['<%%= env.dist %>/**/*.html'],
            css: ['<%%= env.dist %>/<%%= path.css %>/*.css']
        },

        uglify: {
            options: {
                compress: {
                    drop_console: true
                }
            }
        },

        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%%= path.images %>',
                    src: '{,*/}*.{png,jpg,jpeg}',
                    dest: '<%%= env.dist %>/<%%= path.images %>'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%%= path.images %>',
                    src: '{,*/}*.svg',
                    dest: '<%%= env.dist %>/<%%= path.images %>'
                }]
            }
        },

        htmlmin: {
            dist: {
                options: {
                    removeOptionalTags: false
                },
                files: [{
                    expand: true,
                    cwd: '<%%= path.html %>',
	                src: ['{,views/**/*}*.html', '!<%%= path.includes %>/**'],
                    dest: '<%%= env.dist %>'
                }]
            }
        },

        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%%= path.tmp %>/concat/js/',
                        src: ['scripts.js'],
                        dest: '<%%= path.tmp %>/concat/js/'
                    }
                ]
            }
        },

        // Put files not handled in other tasks here
        copy: {
            server: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%%= path.root %>',
                    dest: '<%%= env.build %>',
                    src: [
                        '*.{ico,png}',
                        '.htaccess',
                        'fonts/**',
                        '<%%= path.scripts %>/**/*.js',
                        '<%%= path.images %>/**'
                    ]
                }]
            },
            scripts : {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%%= path.root %>',
                    dest: '<%%= env.build %>',
                    src: ['<%%= path.scripts %>/**']
                }]
            },
	        images : {
		        files: [{
			        expand: true,
			        dot: true,
			        cwd: '<%%= path.root %>',
			        dest: '<%%= env.build %>',
			        src: ['<%%= path.images %>/**']
		        }]
	        },
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%%= path.root %>',
                    dest: '<%%= env.dist %>',
                    src: [
                        '*.{ico,png}',
                        '.htaccess',
                        'fonts/**',
                        '<%%= path.images %>/**'
                    ]
                },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%%= env.build %>',
                        dest: '<%%= env.dist %>',
                        src: 'css/**'
                    }]
            },
            styles: {
                expand: true,
                dot: true,
                cwd: '<%%= env.build %>/<%%= path.css %>',
                dest: '<%%= env.build %>/<%%= path.css %>',
                src: '{,*/}*.css'
            }
        },

        <% if (includeModernizr) { %>modernizr: {
	        options : {
	            uglify: true
	        },
	        dist : {
	            devFile: '<%%= path.bower %>/modernizr/modernizr.js',
	            outputFile: '<%%= env.dist %>/<%%= path.scripts %>/modernizr.js',
	            files: {
                    src : [
	                    '<%%= env.dist %>/<%%= path.scripts %>/{,*/}*.js',
	                    '<%%= env.dist %>/<%%= env.build %>/<%%= path.css %>/{,*/}*.css',
    	            ]
	             }
	          }
	    },
        <% } %>
        concurrent: {
            options: {
                logConcurrentOutput: true,
                limit: 5
            },
            server: [
                'sass:server',
                'copy:styles',
                'copy:server'
            ],
            dist: [
                'sass:dist',
                'copy:dist',
                'imagemin',
                'svgmin'
            ]
        },

        prettify: {
            options: {
                indent: 2,
                indent_char: '  ',
                wrap_line_length: 78,
                brace_style: 'expand',
                unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u']
            },
            files: {
                expand: true,
                cwd: '<%%= env.dist %>',
                src: ['{,*/}*.html'],
                dest: '<%%= env.dist %>',
                ext: '.html'
            }
        }<% if (includeTesting) { %>,

        // Unit / Integration testing modules
        protractor: {
            options: {
               configFile: 'node_modules/protractor/referenceConf.js', // Default config file
                keepAlive: true, // If false, the grunt process stops when the test fails.
                noColor: false, // If true, protractor will not use colors in its output.
                args: {
                // Arguments passed to the command
                }
            },
            dist: {   // Grunt requires at least one target to run so you can simply put 'all: {}' here too.
                options: {
                    configFile: '<%%= path.test %>/protractor.conf.js', // Target-specific config file
                    args: {} // Target-specific arguments
                }
            }
        },

        protractor_webdriver: {
            options : {
                keepAlive: true,
                debug: true
            },
            start: {
                options: {
                    path: 'node_modules/protractor/bin/',
                    command: 'webdriver-manager start'
                }
            },
            stop: {
                options: {
                    path: 'node_modules/protractor/bin/',
                    command: 'webdriver-manager start'
                }
            }
        },

        karma: {
            unit: {
                configFile: '<%%= path.test %>/karma-unit.conf.js',
                autoWatch: false,
                singleRun: true
            },
            unit_auto: {
                configFile: '<%%= path.test %>/karma-unit.conf.js'
            }
        }<%}%>

    });

    // 'GRUNT' starts the prompts to choose different build types
    grunt.registerTask('default',function(target){
        var tasks = ['prompt:global'];
        grunt.task.run(tasks)
    });

    // 'GRUNT SERVER' builds the project to build folder for development and running this on a local webserver
    grunt.registerTask('server', function () {
        gruntServerTasks();
    });

    // 'GRUNT DIST' builds the project to dist(tribution) folder for production (minify etc)
    grunt.registerTask('dist',function (target){
        if (target == 'server') {
            target = true;
        }
        gruntDistTasks(target);
    }) ;

    <% if (includeTesting) { %>// 'GRUNT TEST' run unit test with karma and e2e with protractor
    grunt.registerTask('test', ['karma:unit', 'protractor_webdriver:start', 'protractor:dist', 'protractor_webdriver:stop']);
    grunt.registerTask('test:unit', ['karma:unit']);
    grunt.registerTask('test:e2e', ['protractor_webdriver:start', 'protractor:dist', 'protractor_webdriver:stop']);<%}%>

    function gruntServerTasks() {
        var tasks = [
            'clean:server',
            'concurrent:server',
            'includereplace:server',
            'autoprefixer:server',
            'connect:livereload',
            'watch'
        ];
        return grunt.task.run(tasks);
    }

    function gruntDistTasks(target) {
        var tasks = [
            'prompt:dist',
            'clean:dist',
            'concurrent:dist',
            'htmlmin',
            'includereplace:dist',
            'autoprefixer:dist',
            'useminPrepare',
            'concat',
            'ngAnnotate',
            'uglify',
            'cssmin',<% if (includeModernizr) { %>
            'modernizr',<% } %><% if (includeGruntRev) { %>
            'rev',<% } %>
            'usemin',
            'prettify'
        ];
        if (target) {
            // 'GRUNT DIST:SERVER' builds the project to dist folder for production and running this on a local webserver
            tasks.push('connect:dist:keepalive');
        }
        return grunt.task.run(tasks);
    }

    function registerGruntTask(buildType,target) {
        switch (buildType) {
            case 'server':
                gruntServerTasks();
                break;
            case 'dist' :
                gruntDistTasks(target);
                break;
        }
    }
};
