// LD: Mocha and Chai Unit Testing
//     see: http://mochajs.org/ for more info and documentation (Mocha)
//     see: http://chaijs.com/ for more info and documentation (Chai)
// NOTE: Unit Testing with Mocha is set to TDD-style
//       So instead of 'describe' and 'it' we use 'suite' and 'test'
//       We also use Chai-assert and not Chai-expect

// LD: TEST SUITE NAME could be anything
//     I recommend a clear name of the module / factory / service / filter you are testing
//     e.g.: Factory: testFactory
//           Filter : dateFilter
suite('<< TEST SUITE NAME >>', function() {

    // LD: setup is run before each test
    //     whatever you need to setup / clear before each test (see documentation for more)
    setup( function() {

    });

    // LD: teardown is run after each test
    //     whatever you need to clear / destroy after each test (see documentation for more)
    teardown( function() {

    });

    // LD: the TEST NAME should say what you are testing and what it should do.
    //     e.g.: 'should return true when 1 equals 1'
    //     it then reads: test('should return true when 1 equals 1')
    test('<< TEST NAME >>', function() {

    });
});