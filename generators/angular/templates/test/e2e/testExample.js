// LD: protractor E2E testing
//     see: http://angular.github.io/protractor/#/ for more info and documentation
// NOTE: protractor uses BDD-style

// LD: TEST SUITE NAME could be anything
//     I recommend a clear name of the module / factory / service / filter you are testing
//     e.g.: Factory: testFactory
//           Filter : dateFilter
describe('<< TEST SUITE NAME >>', function() {

    // LD: beforeEach is run before each test
    //     whatever you need to setup / clear before each test (see documentation for more)
    beforeEach( function() {

    });

    // LD: afterEach is run after each test
    //     whatever you need to clear / destroy after each test (see documentation for more)
    afterEach( function() {

    });

    // LD: the TEST NAME should say what you are testing and what it should do.
    //     e.g.: 'should login when user enters details and presses login'
    //     it then reads: it('should login when user enters details and presses login')
    it('<< TEST NAME >>', function() {


    });
});