// conf.js
exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['e2e/testExample.js'],
  multiCapabilities: [
    { 'browserName': 'chrome' },
    { 'browserName': 'firefox'}
  ]
};
