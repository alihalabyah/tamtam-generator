module.exports = function() {
    return {
        basePath: '../',
        frameworks: ['mocha'],
        reporters: [ 'spec', 'bamboo'],
        browsers: ['PhantomJS'], //, 'Chrome', 'Firefox'
        autoWatch: true,

        // These are default values anyway
        singleRun: false,
        colors: true,

        files : [

            // 3rd Party Code
            'bower_components/angular/angular.js',
            'bower_components/angular-mocks/angular-mocks.js',<% if(includeAngularRoute) { %>
            'bower_components/angular-route/angular-route.js',<%}%><% if(includeAngularAnimate) { %>
            'bower_components/angular-animate/angular-animate.js',<%}%><% if(includeAngularResource) { %>
            'bower_components/angular-resource/angular-resource.js',<%}%>

            // App-specific Code
            'js/config.js',
            'js/angular/modules/<%= fileProjectName %>.js',
            'js/angular/modules/**/*.js',
            'js/angular/services/**/*.js',
            'js/angular/directives/**/*.js',
            'js/angular/controllers/**/*.js',
            'js/angular/filters/**/*.js',
            'js/angular/transformers/**/*.js',

            // Test-Specific Code
            'node_modules/chai/chai.js',
            'test/lib/chai-should.js',
            'test/lib/chai-expect.js',
            'test/lib/chai-assert.js'
        ]
    }
};
