'use strict';
var path = require('path');
var util = require('util');
var spawn = require('child_process').spawn;
var chalk = require('chalk');
var yeoman = require('yeoman-generator');
var request = require('request-json');


var GitGenerator = module.exports = function GitGenerator(args, options, config) {
	yeoman.generators.Base.apply(this, arguments);

	this.on('end', function () {

        this.log(chalk.gray.bold("Cloning " + this.repository));
        this.spawnCommand('git', ['clone', this.repository]);
	});
	this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, 'package.json')));

};

var repositoriesLoaded = false;

util.inherits(GitGenerator, yeoman.generators.Base);

GitGenerator.prototype.askFor = function askFor() {
    var cb = this.async(),
        self = this,
        repositories = [],
        unauthorized = true,
        getRepoTimeout,
        output = {};

    function getRepo(response) {

        var credentials = response;
        var pageId = 1;

        function requestGit(page) {
            var client = request.newClient('https://' + credentials.username + ':' + credentials.password + '@bitbucket.org/api/2.0/');
            client.get('repositories/tamtam-nl?page=' + page, requestGitSuccess);
        }

        function requestGitSuccess(error, response, body) {
            if (!error && response.statusCode == 200) {
                unauthorized = false;
                var i,
                    list = body.values;

                for (i in list) {
                    var name = list[i].name + ' - ' + list[i].links.clone[0].href;
                    repositories.push({name: name, value: list[i].links.clone[0].href});
                }

                if (body.next != null){
                    pageId++;
                    requestGit(pageId);
                } else {
                    repositories.push(new yeoman.inquirer.Separator());
                    repositories.sort(function(a, b){
                        return a.name == b.name ? 0 : a.name < b.name ? -1 : 1;
                    });
                    repositoriesLoaded = true;
                    clearTimeout(getRepoTimeout);
                    promptRepositories(output);
                }
            }
            if (response.statusCode == 401) {
                unauthorized = true;
                self.log.error(chalk.red.bold("Unauthorized. Wrong Username or Password"));
            }
        }

        requestGit(pageId);
    }

    function ask() {
        self.prompt({
            type: 'input',
            name: 'username',
            message: 'What\'s your bitbucket username'
        }, function (answer) {
            output.username = answer.username;
            self.prompt({
                type: 'password',
                name: 'password',
                message: 'What\'s your bitbucket password'
            }, function (answer) {
                output.password = answer.password;
                getRepo(output);
                self.log(chalk.green.bold("Getting Repositories..."));

                // todo: Try to make this work with promises
                getRepoTimeout = setTimeout(function ()  {
                    self.log.error(chalk.red.bold("Unable to get repositories"));
                }, 20000);
            });
        });
    }

    function promptRepositories(output) {
        self.prompt({
            when: function () {
                return output.password && repositoriesLoaded;
            },
            type: 'list',
            name: 'repository',
            choices: repositories,
            message: 'Choose repository'

        }, function (answer) {
            self.repository = answer.repository;
            if (unauthorized) {
                ask();
            }
            else {
                cb();
            }
        });
    }
    ask();
};
